###################################################################################
#
# Adapted from Tony Murray's script Search-GPOsForString.ps1
# https://gallery.technet.microsoft.com/scriptcenter/Search-all-GPOs-in-a-b155491c
#
###################################################################################

Import-Module grouppolicy

# Create folder to store GPO XML data
$currentTime = (Get-Date).ToString('yyyy-MM-dd hh.mm.ss')
New-Item -Path .\ -Name ".\$($currentTime)" -ItemType "directory" | Out-Null

# Get the strings we want to search for
$gpoXMLString = Read-Host -Prompt "Input string you wish to search in the GPO XML data"
$gpoDisplayNameString = Read-Host -Prompt "Input string to limit search to specific GPO DisplayName"

# Set the domain to search for GPOs based on the computer's domain
$DNSSuffix = Get-WmiObject -class win32_computersystem | Select-Object -ExpandProperty "Domain"

# Find GPOs whose DN is like what was specified
$gposByDN = Get-GPO -all -domain $DNSSuffix | where displayname -like $gpoDisplayNameString

Write-Host "Starting search...."
# Look through each GPO's XML for the string
# Output the GPO's XML to a file if there is a match
foreach ($gpo in $gposByDN) {
    $report = Get-GPOReport -domain $DNSSuffix -Guid $gpo.Id -ReportType Xml
    if ($report -match $gpoXMLString) {
        write-host "Match found in: $($gpo.DisplayName)"
		$fileName = $gpo.DisplayName
		$fileName = $fileName -replace '[^a-zA-Z0-9 ]', ''
		$filePath = (Join-Path -Path $currentTime -ChildPath $fileName) + ".xml"
		Out-File -FilePath $filePath
		Add-Content -Path $filePath -Value $report
    }
    else {
        Write-Host "No match in: $($gpo.DisplayName)"
    }
}