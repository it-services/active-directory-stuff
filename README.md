# Active Directory Stuff

## Search-GPOString.ps1
Run this script to do a string search on all GPOs in a computer's AD domain (i.e. cntrlsrvs.w2k.vt.edu). 
Alternatively, you can limit the string search within GPOs that match a given DisplayName (i.e. LIB* to only search within GPOs that have a name that begins with "LIB").
The script will create a folder in the path from which it is run with the current date/time, and if there's a string match, it will save the GPO's XML data to that folder, using the DN of the GPO.

