 ### Set our OU ###
$ou = "OU=Security Groups,OU=LIB,OU=Central,DC=cntrlsrvs,DC=w2k,DC=vt,DC=edu"

$Groups = Get-ADGroup -Properties * -Filter 'name -notlike "Print*"' -SearchBase $ou | sort name

$Table = @()

$Record = @{
    "Username" = ""
    "Group Name" = ""
}

Foreach($G In $Groups)
{
    $Arrayofmembers = (Get-ADGroupMember -identity $G.samaccountname | select samaccountname).samaccountname
    $listofmembers = ($Arrayofmembers -join ",")
    $Record."Username" = $listofmembers
    $Record."Group Name" = $G.SamAccountName
    $objRecord = New-Object PSObject -property $Record
    $Table += $objrecord
}

$Table | Export-Csv -Path .\output.csv -Delimiter ';' -NoTypeInformation 
